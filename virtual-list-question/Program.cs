﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace virtual_list_question {
    class Program {
        public class LocalizedTextTable {
            [Key]
            public int Id { get; set; }
            // Navigation properties
            public virtual ICollection<LocalizedText> TextItems { get; set; }
        }

        public class LocalizedText {
            public int Id { get; set; }
            public string Locale { get; set; }

            [Required]
            public string Text { get; set; }

            // Navigation properties
            public virtual LocalizedTextTable Table { get; set; }
        }

        public class Element {
            [Required]
            public int Id { get; set; }
            [Required]
            public string Name { get; set; }
            
            [Required]
            public int TitleId { get; set; }
            [Required]
            public int BriefId { get; set; }
            // Navigation properties
            public virtual LocalizedTextTable Title { get; set; }
            public virtual LocalizedTextTable Brief { get; set; }
        }

        class LocalProjectDB : DbContext {
            public DbSet<LocalizedText> table_LocalizedText { get; set; }
            public DbSet<Element> table_Elements { get; set; }

            protected override void OnModelCreating(ModelBuilder modelBuilder) {
                modelBuilder.Entity<LocalizedText>(e => {
                    e.HasKey(item => new { item.Id, item.Locale });

                    e.HasOne(e => e.Table)
                    .WithMany(e => e.TextItems)
                    .HasForeignKey(e => e.Id);
                });

                modelBuilder.Entity<Element>()
                    .HasOne(e => e.Title)
                    .WithMany()
                    .HasForeignKey(e => e.TitleId)
                    .OnDelete(DeleteBehavior.Restrict);

                modelBuilder.Entity<Element>()
                    .HasOne(e => e.Brief)
                    .WithMany()
                    .HasForeignKey(e => e.BriefId)
                    .OnDelete(DeleteBehavior.Restrict);
            }

            protected override void OnConfiguring(DbContextOptionsBuilder options) {
                options.UseSqlite("Data Source=test.sqlite");
            }
        }

        static void Main() {
            Console.WriteLine("Program started");
            LocalProjectDB db = new LocalProjectDB();
            Console.WriteLine("Object created");
            db.Database.EnsureCreated();
            Console.WriteLine("DB created");
            db.SaveChanges();
            Console.WriteLine("Everything works");
        }
    }
}
